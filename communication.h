#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_


void init_send_address();
void init_rec_address();
void get_aveliable_addresses();
void refresh_menu(menu_t *menu);
void create_communication_menu();
void recieve_request();
void* communication_thread(void *vargp);
void connect_unit(char* ip_address);
void send_data();
void recieve_data();

typedef struct {
	char ip[5][16];
	int ip_amount;
	bool is_sender;
	bool is_connected;
	unsigned long unit_ip;
} com_state_t;

com_state_t com_state;


#endif
