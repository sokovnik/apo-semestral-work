*Autor: Nikita Sokovnin*

*Project: Control of MZAPO board and LED reflectors*

*Subject: B35APO Computer Architechtures*

*Semestr: b182 - letní 2018/2019*


**Introduction**

This projeсt was originated as a semestral work for subject Computer Architectures. The purpose of this project is to create the program for MZAPO board that controls two RGB reflectors and contains functions to interact with user, to create different lighting effects and to communicate with other units.



**Installation**

For installation you need to download source code from git repository, then compile and run on MicroZed.

Required software:
* Git client
* Arm linux C compilator with minimal supported version С99



Installation steps:
1. Open console
2. Clone git repository with command:
 git clone git@gitlab.fel.cvut.cz:sokovnik/apo-semestral-work.git
3. In the project folder compile and run project with command:
 make run TARGET_IP=192.168.202.[last 3 numbers of board IP]


If everything went well, you should see main menu of program on the board LCD. Now you are ready to start controlling the light.

**Control**

Entering input parameters and navigating in the menu is possible by using three rotary knobs marked ENCODER1 (red), ENCODER2 (green), and ENCODER3 (blue). Also there are buttons on the knobs: blue to select/apply changes, red to back/cancel.

* To navigate in the menu rotate green knob
* To select a menu item press blue button
* To go back press red button
* For choosing color use all 3 knobs, to enter RGB value
* For choosing time rotate green knob
* For saving changes press blue button
* To forget changes and return to previous state press red button

