#ifndef _LCDFRAME_H_
#define _LCDFRAME_H_

#include <stdint.h>
#include <stdbool.h>
#include "utils.h"

#define FRAME_H 320
#define FRAME_W 480
#define BACKGROUND_COLOR 0x0
#define TEXT_COLOR 0xFFFF
#define SELECTED_TEXT_COLOR 0x6445
#define INTERFACE_COLOR 0x2125
#define FIRST_COLOR 0x4249
#define SECOND_COLOR 0x6B6E
#define BIG_LETTER_SHIFT 4
#define LITTLE_LETTER_SHIFT 12
#define REC_HEIGHT 40
#define COLOR_REC_WIDTH 80
#define LEFT_COLUMN 20
#define RIGHT_COLUMN 300

typedef struct {
	int x;
	int y;
	int width;
	int height;
	uint16_t color;
} rectangle_t;

bool is_big_font;

extern unsigned char *parlcd_mem_base;

extern uint16_t frame [FRAME_H][FRAME_W];

void frameToLCD();
int charToFrame(char c, int yRow, int xColumn, uint16_t forecolor, uint16_t backcolor);
int bigCharToFrame(char c, int yRow, int xColumn, uint16_t forecolor, uint16_t backcolor);
int strToFrame(char *str, int yRow, int xColumn, uint16_t forecolor, uint16_t backcolor, bool is_big);
void draw_background();
void draw_select_button(int y_shift);
void draw_back_button(int y_shift);
void rectangleToFrame(rectangle_t rec);
void draw_interface();
rectangle_t create_rectangle(int x, int y, int width, int height, uint16_t color);

#endif
