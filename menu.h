#ifndef _MENU_H_
#define _MENU_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "lcdframe.h"
#include "utils.h"
#include "communication.h"
#include "effects.h"


extern uint16_t frame[FRAME_H][FRAME_W];
extern unsigned char *parlcd_mem_base;
unsigned char *mem_base;
unsigned char *knobs;
unsigned char *led_line;
uint32_t *bar;
bool blue_button_pressed;
bool red_button_pressed;

void create_main_menu();
void create_static_light_menu();
void create_color_flashing_menu();
void create_color_flashing_select_menu(int option);
void create_continuous_changing_menu();
void create_flashing_menu();
void create_connection_screen();
void initialize_com_menu();
void create_effects_menu();

led_t led1, led2;
menu_t com_menu;
knobs_values_t knobs_values;
int green_knob_prev;
char* current_unit;

void update_knobs_values(knobs_values_t *knobs_values);
void initialize_led(led_t *led);
void set_led_color(led_t *led);
void* main_thread(void *vargp);
void* led_thread(void *vargp);
void* bar_thread(void *vargp);
void apply_continuous_changing(rgb_value_t rgb_value1, rgb_value_t rgb_value2, int change_time, int option);
void draw_menu(menu_t menu);

#endif
