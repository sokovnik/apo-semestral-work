#include "menu.h"

void* main_thread(void *vargp);
void* led_thread(void *vargp);
void* bar_thread(void *vargp);
void init_mem_pointers();

int main(int argc, char *argv[])
{
	init_mem_pointers();
	pthread_t thread_id;
	pthread_create(&thread_id, NULL, main_thread, NULL);
	pthread_create(&thread_id, NULL, led_thread, NULL);
	pthread_create(&thread_id, NULL, bar_thread, NULL);
	pthread_create(&thread_id, NULL, communication_thread, NULL);
	pthread_exit(NULL);
	return 0;
}

void* main_thread(void *vargp) {
	current_unit = "This";
	while (true) {
		create_main_menu();
	}
	return NULL;
}

void* led_thread(void *vargp) {
	while (true) {
		set_led_color(&led1);
		set_led_color(&led2);
	}
	return NULL;
}

void* bar_thread(void *vargp) {
	uint32_t bar_value = 0x80000000;
	bool direction = true;
	double start_time = (double)clock() / CLOCKS_PER_SEC * 1000;
	double cur_time;
	while (true) {
		cur_time = (double)clock() / CLOCKS_PER_SEC * 1000;
		if (cur_time - start_time > 50) {
			*bar = bar_value;
			if (direction) {
				bar_value = bar_value >> 1;
				if (bar_value == 0) {
					bar_value = 1;
					direction = false;
				}
			} else {
				bar_value = bar_value << 1;
				if (bar_value == 0) {
					bar_value = 0x80000000;
					direction = true;
				}
			}
			start_time = cur_time;
		}
	}
}

void init_mem_pointers() {
	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	knobs = mem_base + SPILED_REG_KNOBS_8BIT_o;
	led_line = mem_base + SPILED_REG_LED_LINE_o;
	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	bar = map_phys_address(SPILED_REG_BASE_PHYS + SPILED_REG_LED_LINE_o, 4, false);
	initialize_led(&led1);
	initialize_led(&led2);
	led1.led_mem = mem_base + SPILED_REG_LED_RGB1_o;
	led2.led_mem = mem_base + SPILED_REG_LED_RGB2_o;
}
