#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>

#include "lcdframe.h"
#include "utils.h"
#include "communication.h"

#define PORT 7777
#define BUFFERSIZE 1024
#define IP_REQUEST 'r'
#define ACCEPT_IP_REQUEST 'y'
#define CONNECTION_REQUEST 'c'
#define ACCEPT_CONNECTION_REQUEST 's'
#define DISCONNECT 'd'

struct sockaddr_in send_addr, rec_addr, src_addr;
int sockfd;
char buffer[BUFFERSIZE];

void* communication_thread(void *vargp) {
	init_send_address();
	while (true) {
		if (!(com_state.is_sender)) {
			if (!(com_state.is_connected)) {
				recieve_request();
			} else {
				recieve_data();
			}
		} else {
			if (com_state.is_connected) {
				init_rec_address(com_state.unit_ip);
				send_data();
			}
		}
	}
	return NULL;
}

void init_send_address() {
	com_state.is_sender = false;
	if ((sockfd = socket (AF_INET, SOCK_DGRAM, 0)) == -1) {
		printf("Socket error");
		exit(1);
    }
    
	memset(&send_addr, 0, sizeof(send_addr));
	send_addr.sin_family = AF_INET;
	send_addr.sin_port = htons(PORT);
	send_addr.sin_addr.s_addr = INADDR_ANY;
	
	int yes = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
		    sizeof(yes)) == -1) {
		perror("Set options error");
		exit(1);
	}
	
	if (bind(sockfd, (struct sockaddr *)&send_addr, sizeof(send_addr)) == -1) {
		printf("Bind error");
		exit(1);
	}
	
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 100000;
	if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
		printf("Set timeout error");
	}

	int so_broadcast = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &so_broadcast, sizeof(so_broadcast)) < 0) {
		printf("Set options error");
		exit(1);
	}
}

void init_rec_address(unsigned long IP) {
	rec_addr.sin_family = AF_INET;
	rec_addr.sin_port = htons(PORT);
	rec_addr.sin_addr.s_addr = IP;
}

void send_ip_request() {
	printf("Send ip request\n");
	init_rec_address(INADDR_BROADCAST);
	char msg = IP_REQUEST;
	sendto(sockfd, &msg, sizeof(char), 0, (const struct sockaddr *)&rec_addr, sizeof(rec_addr)); 
}

void get_address() {
	int msg_len;
	unsigned int rec_addr_len;
	if((msg_len = recvfrom(sockfd, buffer, BUFFERSIZE, 0, (struct sockaddr*)&rec_addr, &rec_addr_len)) > 0) {
		if (buffer[0] == ACCEPT_IP_REQUEST) {
			printf("Ip request accepted\n");
			strcpy(com_state.ip[(com_state.ip_amount)++], inet_ntoa(rec_addr.sin_addr));
		}
	}
}

void get_aveliable_addresses() {
	com_state.ip_amount = 0;
	send_ip_request();
	get_address();
	get_address();
	get_address();
	get_address();
	get_address();
}

void recieve_request() {
	int msg_len;
	unsigned int src_addr_len;
	if((msg_len = recvfrom(sockfd, buffer, BUFFERSIZE, 0, (struct sockaddr*)&send_addr, &src_addr_len)) > 0) {
		if (buffer[0] == IP_REQUEST) {
			printf("Ip request recieved!\n");
			char msg = ACCEPT_IP_REQUEST;
			printf("Sending accept to address : %s\n", inet_ntoa(send_addr.sin_addr));
	 		sendto(sockfd, &msg, sizeof(char), 0, (const struct sockaddr *)&send_addr, sizeof(send_addr));
		}
		
		if (buffer[0] == CONNECTION_REQUEST) {
			printf("Connection request recieved!\n");
			com_state.is_connected = true;
		}
	}
}

void send_connection_request(unsigned long ip_address) {
	init_rec_address(ip_address);
	char msg = CONNECTION_REQUEST;
	sendto(sockfd, &msg, sizeof(char), 0, (const struct sockaddr *)&rec_addr, sizeof(rec_addr)); 
}

void send_data() {
	char msg = CONNECTION_REQUEST;
	while (com_state.is_connected) {
		if (led1.mode_changed || led2.mode_changed) {
			printf("%d %d\n", led1.mode_changed, led2.mode_changed);
			led1.mode_changed = false;
			led2.mode_changed = false;
			printf("Sending\n");
			memcpy(buffer, (char*)&led1, sizeof(led1));
			memcpy(buffer + sizeof(led1), (char*)&led2, sizeof(led2));
			printf("Sending to %s\n", inet_ntoa(rec_addr.sin_addr));
			sendto(sockfd, buffer, sizeof(led1) + sizeof(led2), 0, (struct sockaddr *)
		 &rec_addr, sizeof(rec_addr));
		} else {
			sendto(sockfd, &msg, sizeof(char), 0, (const struct sockaddr *)&rec_addr, sizeof(rec_addr));
		}
	}
}

void actualize_time() {
	double change_time = ((double)clock()) / CLOCKS_PER_SEC * 1000;
	led1.flashing.prev_change_time = change_time;
	led2.flashing.prev_change_time = change_time;
	led1.color_flashing.prev_change_time = change_time;
	led2.color_flashing.prev_change_time = change_time;
	led1.flashing.prev_change_time = change_time;
	led2.flashing.prev_change_time = change_time;
	led1.cont_change.prev_change_time_h = change_time;
	led1.cont_change.prev_change_time_s = change_time;
	led1.cont_change.prev_change_time_v = change_time;
	led2.cont_change.prev_change_time_h = change_time;
	led2.cont_change.prev_change_time_s = change_time;
	led2.cont_change.prev_change_time_v = change_time;
}

void recieve_data() {
	int msg_len;
	unsigned int src_addr_len;
	unsigned char *led_mem_temp;
	while (true) {
		printf("Recieving!\n");
		if ((msg_len = recvfrom(sockfd, buffer, BUFFERSIZE, 0, (struct sockaddr *) &send_addr, &src_addr_len)) > 100) {
			printf("Leds recieved + %d\n", msg_len);
			led_mem_temp = led1.led_mem;
			memcpy((char*)&led1, buffer, sizeof(led1));
			led1.led_mem = led_mem_temp;
			led_mem_temp = led2.led_mem;
			memcpy((char*)&led2, buffer + sizeof(led1), sizeof(led2));
			led2.led_mem = led_mem_temp;
			actualize_time();
			com_state.is_connected = true;
		} else if (msg_len > 0) {
			com_state.is_connected = true;
		} else {
			com_state.is_connected = false;
		}
		printf("msglen + %d\n", msg_len);
	}
	printf("Disconnect!\n");
}
	

void connect_unit(char* ip_address) {
	printf("Connect to %s\n", ip_address);
	send_connection_request(inet_addr(ip_address));
	com_state.is_connected = true;
	com_state.unit_ip = inet_addr(ip_address);
}

void refresh_menu(menu_t *menu) {
	for (int i = 0; i < com_state.ip_amount; i++) {
		menu->buttons[i + 1] = com_state.ip[i];
	}
	menu->buttons_num = com_state.ip_amount + 1;
}

