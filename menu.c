#include "menu.h"

void draw_menu(menu_t menu) {
	//draw_background();
	draw_interface();
	int y_shift = is_big_font ? BIG_LETTER_SHIFT : LITTLE_LETTER_SHIFT;
	strToFrame(menu.name, y_shift, 10, TEXT_COLOR, INTERFACE_COLOR, is_big_font);
	strToFrame(current_unit, y_shift, 200, TEXT_COLOR, INTERFACE_COLOR, is_big_font);
	uint16_t back_color, text_color;
	for (int i = 0; i < menu.buttons_num; i++) {
		back_color = i % 2 == 0 ? FIRST_COLOR : SECOND_COLOR;
		text_color = i == menu.selected ? SELECTED_TEXT_COLOR : TEXT_COLOR;
		strToFrame(menu.buttons[i], y_shift + (i + 1) * REC_HEIGHT, LEFT_COLUMN, text_color, back_color, is_big_font);
	}
	draw_select_button(y_shift);
	if (strcmp(menu.name, "Main menu") != 0) {
		draw_back_button(y_shift);
	}
	frameToLCD();
}

void update_knobs_values(knobs_values_t *knobs_values) {
	uint32_t value = *(volatile uint32_t*)knobs;
	knobs_values->knobs_value = value;
	knobs_values->blue_knob = value & 0xFF;
	knobs_values->green_knob = (value>>8)  & 0xFF;
	knobs_values->red_knob = (value>>16) & 0xFF;
	knobs_values->blue_button = (value>>24) & 1;
	knobs_values->green_button = (value>>25) & 1;
	knobs_values->red_button = (value>>26) & 1;
}

void check_pressed_buttons(knobs_values_t knobs_values) {
	if (blue_button_pressed && knobs_values.blue_button == 0) {
		blue_button_pressed = false;
	}
	if (red_button_pressed && knobs_values.red_button == 0) {
		red_button_pressed = false;
	}
}
void initialize_com_menu() {
	com_state.is_sender = true;
	com_menu.name = "Devices";
	com_menu.selected = 0;
	com_menu.buttons[0] = "This";
	com_menu.buttons_num = 1;
	get_aveliable_addresses();
	refresh_menu(&com_menu);
	get_aveliable_addresses();
	refresh_menu(&com_menu);
}


void create_communication_menu() {
	if (com_menu.buttons_num == 0) {
		initialize_com_menu();
	}
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	while(true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			if (com_menu.selected == 0) {
				if (com_state.is_connected) {
					com_state.is_connected = false;
					led1 = led1_saved;
					led2 = led2_saved;
					current_unit = "This";
				}
			} else {
				if (!(com_state.is_connected)) {
					led1_saved = led1;
					led2_saved = led2;
					led1.mode_changed = false;
					led2.mode_changed = false;
					connect_unit(com_state.ip[com_menu.selected - 1]);
					current_unit = com_menu.buttons[com_menu.selected];
				}
			}
		}
		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&com_menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(com_menu);
	}
}

void create_connection_screen() {
	while (com_state.is_connected) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		draw_background();
		strToFrame("Device is connected", 100, 100, TEXT_COLOR, BACKGROUND_COLOR, is_big_font);
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
		}
		frameToLCD();
	}
}

void create_main_menu() {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Main menu";
	menu.selected = 0;
	menu.buttons[0] = "Light effects";
	menu.buttons[1] = "Choose device";
	menu.buttons[2] = "Change text size";
	menu.buttons_num = 3;
	while(true) {
		if (!(com_state.is_sender) && com_state.is_connected) {
			create_connection_screen();
		}
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			switch (menu.selected) {
				case 0:
					create_effects_menu();
					break;
				case 1:
					create_communication_menu();
					break;
				case 2:
					is_big_font = !is_big_font;
					break;
			}
		}
		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(menu);
	}
}

void create_effects_menu() {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Effects";
	menu.selected = 0;
	menu.buttons[0] = "Static light";
	menu.buttons[1] = "Continuous change";
	menu.buttons[2] = "Color flashing";
	menu.buttons[3] = "Flashing";
	menu.buttons_num = 4;
	while(true) {
		if (!(com_state.is_sender) && com_state.is_connected) {
			create_connection_screen();
		}
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			switch (menu.selected) {
				case 0:
					create_static_light_menu();
					break;
				case 1:
					create_continuous_changing_menu();
					break;
				case 2:
					create_color_flashing_menu();
					break;
				case 3:
					create_flashing_menu();
					break;
			}
		}
		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(menu);
	}
}

void light(led_t *led) {
	if (!(com_state.is_connected && com_state.is_sender)) {
		*(volatile char*)led->led_mem = led->cur_rgb_value.blue;
		*(volatile char*)(led->led_mem + 1) = led->cur_rgb_value.green;
		*(volatile char*)(led->led_mem + 2) = led->cur_rgb_value.red;
	}
}

void select_led_color(int option, mode_type *mode1, mode_type *mode2, int color) {
	mode_type mode1_saved = *mode1;
	mode1->on = true;
	mode_type mode2_saved;
	if (mode2 != NULL) {
		mode2_saved = *mode2;
		mode2->on = true;
	}
	rectangle_t rec;
	rec.x = RIGHT_COLUMN;
	rec.y = REC_HEIGHT + option * REC_HEIGHT;
	rec.width = COLOR_REC_WIDTH;
	rec.height = REC_HEIGHT;
	knobs_values_t prev_knobs_values;
	update_knobs_values(&prev_knobs_values);

	while(true) {
		update_knobs_values(&knobs_values);
		if (color == 1) {
			update_rgb_value(&knobs_values, &prev_knobs_values, &(mode1->rgb_value1));
			mode1->hsv_value1 = convert_rgb_to_hsv(mode1->rgb_value1);
			rec.color = get_rgb565(mode1->rgb_value1);
			if (mode2 != NULL) {
				mode2->rgb_value1 = mode1->rgb_value1;
				mode2->hsv_value1 = mode1->hsv_value1;
			}
		} else if (color == 2) {
			update_rgb_value(&knobs_values, &prev_knobs_values, &(mode1->rgb_value2));
			mode1->hsv_value2 = convert_rgb_to_hsv(mode1->rgb_value2);
			rec.color = get_rgb565(mode1->rgb_value2);
			if (mode2 != NULL) {
				mode2->rgb_value2 = mode1->rgb_value2;
				mode2->hsv_value2 = mode1->hsv_value2;
			}
		}
		rectangleToFrame(rec);
		check_pressed_buttons(knobs_values);
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			*mode1 = mode1_saved;
			light_off(&led1);
			light_off(&led2);
			if (mode2 != NULL) {
				*mode2 = mode2_saved;
			}
			return;
		}
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			return;
		}
		frameToLCD();
	}
}

void create_static_light_menu() {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Static";
	menu.selected = 0;
	menu.buttons[0] = "First reflector";
	menu.buttons[1] = "Second reflector";
	menu.buttons[2] = "Both reflectors";
	menu.buttons[3] = "Copy from 1 to 2";
	menu.buttons[4] = "Copy from 2 to 1";
	menu.buttons[5] = "On/off";
	menu.buttons_num = 6;
	while (true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			switch (menu.selected) {
				case 0:
					//led1.static_light.on = true;
					select_led_color(0, &(led1.static_light), NULL, 1);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 1:
					//led2.static_light.on = true;
					select_led_color(1, &(led2.static_light), NULL, 1);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 2:
					led1.static_light.on = true;
					led2.static_light.on = true;
					select_led_color(2, &(led1.static_light), &(led2.static_light), 1);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 3:
					copy_led_color(&led1, &led2);
					break;
				case 4:
					copy_led_color(&led2, &led1);
					break;
				case 5:
					if (led1.static_light.on || led2.static_light.on) {
						led1.static_light.on = false;
						led2.static_light.on = false;
						light_off(&led1);
						light_off(&led2);
					} else {
						led1.static_light.on = true;
						led2.static_light.on = true;
					}

			}
			led1.mode_changed = true;
			led2.mode_changed = true;
		}
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}

		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(menu);
	}
}

void create_color_flashing_menu() {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Color flash";
	menu.selected = 0;
	menu.buttons[0] = "First reflector";
	menu.buttons[1] = "Second reflector";
	menu.buttons[2] = "Both reflectors";
	menu.buttons[3] = "Both in anti-phase";
	menu.buttons_num = 4;
	while (true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			create_color_flashing_select_menu(menu.selected);
		}
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}

		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(menu);
	}
}

rgb_value_t select_color(int option) {
	uint16_t color;
	rectangle_t rec;
	rec.x = RIGHT_COLUMN;
	rec.y = REC_HEIGHT + option * REC_HEIGHT;
	rec.width = COLOR_REC_WIDTH;
	rec.height = REC_HEIGHT;
	knobs_values_t prev_knobs_values;
	update_knobs_values(&prev_knobs_values);
	rgb_value_t rgb_value = {0, 0, 0};
	rgb_value_t return_value = {0, 0, 0};
	while(true) {
		update_knobs_values(&knobs_values);
		update_rgb_value(&knobs_values, &prev_knobs_values, &rgb_value);
		color = get_rgb565(rgb_value);
		rec.color = color;
		rectangleToFrame(rec);
		check_pressed_buttons(knobs_values);
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return return_value;
		}
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			return rgb_value;
		}
		frameToLCD();
	}
}

void set_time(int *time, int option) {
	int y_shift = is_big_font ? BIG_LETTER_SHIFT : LITTLE_LETTER_SHIFT;
	int back_color = option % 2 == 0 ? FIRST_COLOR : SECOND_COLOR;
	int new_time = *time;
	rectangle_t rec;
	rec.x = RIGHT_COLUMN;
	rec.y = REC_HEIGHT * (option + 1);
	rec.width = FRAME_W - RIGHT_COLUMN;
	rec.height = REC_HEIGHT;
	rec.color = back_color;
	char change_time_str[12];
	knobs_values_t prev_knobs_values;
	update_knobs_values(&prev_knobs_values);
	while(true) {
		update_knobs_values(&knobs_values);
		if (knobs_values.green_knob != prev_knobs_values.green_knob) {
			int change = abs(knobs_values.green_knob - prev_knobs_values.green_knob);
			change = change > 200 ? 255 - change : change;
			change = change / 4 * 100;
			if (is_increased(knobs_values.green_knob, prev_knobs_values.green_knob)) {
				new_time += change;
				prev_knobs_values = knobs_values;
			} else if (is_decreased(knobs_values.green_knob, prev_knobs_values.green_knob)){
				new_time -= change;
				new_time = new_time < 0 ? 0 : new_time;
				prev_knobs_values = knobs_values;
			}
		}
		sprintf(change_time_str, "%d", new_time);
		int length = strlen(change_time_str);
		change_time_str[length] = 'm';
		change_time_str[length + 1] = 's';
		change_time_str[length + 2] = '\0';

		rectangleToFrame(rec);
		strToFrame(change_time_str, rec.y + y_shift, RIGHT_COLUMN, TEXT_COLOR, back_color, is_big_font);
		check_pressed_buttons(knobs_values);
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			*time = new_time;
			return;
		}
		frameToLCD();
	}

}

void on_off_color_flashing(int option) {
	double start_time_ms = ((double)clock()) / CLOCKS_PER_SEC * 1000;
	if (option > 1) {
		if (led1.color_flashing.on || led2.color_flashing.on) {
			led1.color_flashing.on = false;
			led2.color_flashing.on = false;
			light_off(&led1);
			light_off(&led2);
		} else {
			led1.color_flashing.prev_change_time = start_time_ms;
			led1.color_flashing.on = true;
			led1.static_light.on = false;
			led1.color_flashing.color_changed = false;
			led2.color_flashing.prev_change_time = start_time_ms;
			led2.color_flashing.on = true;
			led2.static_light.on = false;
			led2.color_flashing.color_changed = false;
		}
	} else {
		if (option == 0) {
			if (led1.color_flashing.on == false) {
				led1.color_flashing.prev_change_time = start_time_ms;
				led1.color_flashing.on = true;
				led1.static_light.on = false;
				led1.color_flashing.color_changed = false;
			} else {
				led1.color_flashing.on = false;
				light_off(&led1);
			}
		}
		if (option == 1) {
			if (led2.color_flashing.on == false) {
				led2.color_flashing.prev_change_time = start_time_ms;
				led2.color_flashing.on = true;
				led2.static_light.on = false;
				led2.color_flashing.color_changed = false;
			} else {
				led2.color_flashing.on = false;
				light_off(&led2);
			}
		}
	}
	if (option == 3) {
			led1.color_flashing.color_changed = true;
	}
}

void create_color_flashing_select_menu(int option) {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Options";
	int change_time = 0;
	menu.selected = 0;
	menu.buttons[0] = "First color";
	menu.buttons[1] = "Second color";
	menu.buttons[2] = "Change time";
	menu.buttons[3] = "On/off";
	menu.buttons_num = 4;
	mode_type *color_flashing;
	if (option == 0) {
		color_flashing = &(led1.color_flashing);
		change_time = led1.color_flashing.change_time;
	} else if (option == 1) {
		color_flashing = &(led2.color_flashing);
		change_time = led2.color_flashing.change_time;
	} else {
		change_time = led1.color_flashing.change_time;
	}

	while (true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			switch (menu.selected) {
				case 0:
					if (option > 1) {
						select_led_color(0, &(led1.color_flashing), &(led2.color_flashing), 1);
					} else {
						select_led_color(0, color_flashing, NULL, 1);
					}
					green_knob_prev = knobs_values.green_knob;
					break;
				case 1:
					if (option > 1) {
						select_led_color(1, &(led1.color_flashing), &(led2.color_flashing), 2);
					} else {
						select_led_color(1, color_flashing, NULL, 2);
					}
					green_knob_prev = knobs_values.green_knob;
					break;
				case 2:
					set_time(&change_time, menu.selected);
					if (option > 1) {
						led1.color_flashing.change_time = change_time;
						led2.color_flashing.change_time = change_time;
					} else {
						color_flashing->change_time = change_time;
					}
					green_knob_prev = knobs_values.green_knob;
					break;
				case 3:
					on_off_color_flashing(option);
					led1.mode_changed = true;
					led2.mode_changed = true;
					break;
			}
		}
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}

		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(menu);
	}
}

void create_continuous_select_menu(int option) {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Options";
	menu.selected = 0;
	menu.buttons[0] = "First color";
	menu.buttons[1] = "Second color";
	menu.buttons[2] = "Change time";
	menu.buttons[3] = "Apply";
	menu.buttons[4] = "Off";
	menu.buttons_num = 5;
	rgb_value_t rgb_value1 = {0, 0, 0};
	rgb_value_t rgb_value2 = {0, 0, 0};
	int change_time = 0;
	while (true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			switch (menu.selected) {
				case 0:
					rgb_value1 = select_color(0);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 1:
					rgb_value2 = select_color(1);
					green_knob_prev= knobs_values.green_knob;
					break;
				case 2:
					set_time(&change_time, menu.selected);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 3:
					if (change_time != 0) {
						apply_continuous_changing(rgb_value1, rgb_value2, change_time, option);
						led1.mode_changed = true;
						led2.mode_changed = true;
					}
					break;
				case 4:
					if (option != 1) {
						led1.cont_change.on = false;
						light_off(&led1);
					}
					if (option != 0) {
						led2.cont_change.on = false;
						light_off(&led2);
					}
					if (option > 1) {
						light_off(&led1);
						light_off(&led2);
					}
					led1.mode_changed = true;
					led2.mode_changed = true;
					break;
			}
		}
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}

		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(menu);
	}
}

void apply_continuous_changing(rgb_value_t rgb_value1, rgb_value_t rgb_value2, int change_time, int option) {
	hsv_value_t hsv_value1 = convert_rgb_to_hsv(rgb_value1);
	hsv_value_t hsv_value2 = convert_rgb_to_hsv(rgb_value2);
	double start_time_ms = ((double)clock()) / CLOCKS_PER_SEC * 1000;
	if (option != 1) {
		led1.cont_change.hsv_value1 = hsv_value1;
		led1.cont_change.hsv_value2 = hsv_value2;
		led1.cont_change.rgb_value1 = rgb_value1;
		led1.cont_change.rgb_value2 = rgb_value2;
		led1.cont_change.cur_rgb_value = rgb_value1;
		led1.cont_change.cur_hsv_value = hsv_value1;
		led1.cont_change.prev_change_time_h = start_time_ms;
		led1.cont_change.prev_change_time_s = start_time_ms;
		led1.cont_change.prev_change_time_v = start_time_ms;
		led1.cont_change.change_time = change_time;
		led1.cont_change.on = true;
		led1.static_light.on = false;
		led1.color_flashing.on = false;
		led1.cont_change.color_changed = false;
	}
	if (option != 0) {
		led2.cont_change.hsv_value1 = hsv_value1;
		led2.cont_change.hsv_value2 = hsv_value2;
		led2.cont_change.rgb_value1 = rgb_value1;
		led2.cont_change.rgb_value2 = rgb_value2;
		led2.cont_change.cur_rgb_value = rgb_value1;
		led2.cont_change.cur_hsv_value = hsv_value1;
		led2.cont_change.prev_change_time_h = start_time_ms;
		led2.cont_change.prev_change_time_s = start_time_ms;
		led2.cont_change.prev_change_time_v = start_time_ms;
		led2.cont_change.change_time = change_time;
		led2.cont_change.on = true;
		led2.static_light.on = false;
		led2.color_flashing.on = false;
		led2.cont_change.color_changed = false;
	}
	if (option == 3) {
		led2.cont_change.hsv_value1 = hsv_value2;
		led2.cont_change.hsv_value2 = hsv_value1;
		led2.cont_change.rgb_value1 = rgb_value2;
		led2.cont_change.rgb_value2 = rgb_value1;
		led2.cont_change.cur_rgb_value = rgb_value2;
		led2.cont_change.cur_hsv_value = hsv_value2;
	}
}

void create_continuous_changing_menu() {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Continuous";
	menu.selected = 0;
	menu.buttons[0] = "First reflector";
	menu.buttons[1] = "Second reflector";
	menu.buttons[2] = "Both reflectors";
	menu.buttons[3] = "Both in anti-phase";
	menu.buttons_num = 4;
	while (true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			create_continuous_select_menu(menu.selected);
		}
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}

		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}

		draw_menu(menu);
	}
}

void set_shift(led_t *led, int shift_time) {
	int total_time = led->flashing.extinction_time + led->flashing.duration_time;
	if (total_time != 0) {
		shift_time %= (total_time);
		if (shift_time > led->flashing.duration_time) {
			led->flashing.color_changed = true;
			shift_time -= led->flashing.duration_time;
		}
		led->flashing.prev_change_time -= shift_time;
	}
}

void create_flashing_select_menu(int option) {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Options";
	menu.selected = 0;
	menu.buttons[0] = "Duration time";
	menu.buttons[1] = "Extinction time";
	menu.buttons[2] = "Shift";
	menu.buttons[3] = "Apply";
	menu.buttons[4] = "Off";
	menu.buttons_num = 5;
	int duration_time = 0;
	int extinction_time = 0;
	int shift_time = 0;
	while (true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			switch (menu.selected) {
				case 0:
					set_time(&duration_time, menu.selected);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 1:
					set_time(&extinction_time, menu.selected);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 2:
					set_time(&shift_time, menu.selected);
					green_knob_prev = knobs_values.green_knob;
					break;
				case 3:
					if (option != 1) {
						led1.flashing.duration_time = duration_time;
						led1.flashing.extinction_time = extinction_time;
						printf("%d\n", led1.flashing.duration_time);
						if (duration_time != 0) {
							led1.flashing.on = true;
						}
						led1.flashing.prev_change_time = ((double)clock()) / CLOCKS_PER_SEC * 1000;
						set_shift(&led1, shift_time);
					}
					if (option != 0) {
						led2.flashing.duration_time = duration_time;
						led2.flashing.extinction_time = extinction_time;
						if (duration_time != 0) {
							led2.flashing.on = true;
						}
						led2.flashing.prev_change_time = ((double)clock()) / CLOCKS_PER_SEC * 1000;
						set_shift(&led2, shift_time);
					}
					led1.mode_changed = true;
					led2.mode_changed = true;
					break;
				case 4:
					if (option != 1) {
						led1.flashing.on = false;
					}
					if (option != 0) {
						led2.flashing.on = false;
					}
					led1.mode_changed = true;
					led2.mode_changed = true;
					break;
			}
		}
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}

		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}
		draw_menu(menu);
	}
}

void create_flashing_menu() {
	update_knobs_values(&knobs_values);
	green_knob_prev = knobs_values.green_knob;
	menu_t menu;
	menu.name = "Flashing";
	menu.selected = 0;
	menu.buttons[0] = "First reflector";
	menu.buttons[1] = "Second reflector";
	menu.buttons[2] = "Both reflectors";
	menu.buttons_num = 3;
	while (true) {
		update_knobs_values(&knobs_values);
		check_pressed_buttons(knobs_values);
		if (knobs_values.blue_button == 1 && !blue_button_pressed) {
			blue_button_pressed = true;
			create_flashing_select_menu(menu.selected);
		}
		if (knobs_values.red_button == 1 && !red_button_pressed) {
			red_button_pressed = true;
			return;
		}

		if (knobs_values.green_knob != green_knob_prev) {
			select_button(&menu, knobs_values.green_knob, &green_knob_prev);
		}

		draw_menu(menu);
	}
}
