#ifndef _EFFECTS_H_
#define _EFFECTS_H_

#include <time.h>

#include "utils.h"
void color_flash (led_t* led);

void flash (led_t* led);

void change(led_t* led);

#endif
