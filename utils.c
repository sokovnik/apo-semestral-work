
#include "utils.h"

void initialize_mode(mode_type *mode) {
	mode->hsv_value1.h = 0;
	mode->hsv_value1.s = 0;
	mode->hsv_value1.v = 0;
	mode->hsv_value2.h = 0;
	mode->hsv_value2.s = 0;
	mode->hsv_value2.v = 0;
	mode->rgb_value1.red = 0;
	mode->rgb_value1.green = 0;
	mode->rgb_value1.blue = 0;
	mode->rgb_value2.red = 0;
	mode->rgb_value2.green = 0;
	mode->rgb_value2.blue = 0;
	mode->change_time = 0;
	mode->prev_change_time = 0;
	mode->color_changed = false;
	mode->on = false;
	mode->duration_time = 0;
	mode->extinction_time = 0;
}

void initialize_led(led_t *led) {
	led->cur_hsv_value.h = 0;
	led->cur_hsv_value.s = 0;
	led->cur_hsv_value.v = 0;
	led->cur_rgb_value.red = 0;
	led->cur_rgb_value.green = 0;
	led->cur_rgb_value.blue = 0;
	led->mode_changed = false;
	initialize_mode(&(led->static_light));
	initialize_mode(&(led->color_flashing));
	initialize_mode(&(led->flashing));
	initialize_mode(&(led->cont_change));
}

void light_off(led_t *led) {
	led->cur_rgb_value.red = 0;
	led->cur_rgb_value.green = 0;
	led->cur_rgb_value.blue = 0;
	led->cur_hsv_value.h = 0;
	led->cur_hsv_value.s = 0;
	led->cur_hsv_value.v = 0;

}

void set_led_color(led_t *led) {
	if (led->static_light.on) {
		led->cur_rgb_value = led->static_light.rgb_value1;
		led->cur_hsv_value = led->static_light.hsv_value1;
	} else if (led->color_flashing.on) {
		color_flash(led);
	} else if (led->cont_change.on) {
		change(led);
	}
	if (led->flashing.on) {
		flash(led);
	}
	light(led);
}

void update_element(int *knob_value, int *prev_knob_value, unsigned char *element) {
	int change = abs(*knob_value - *prev_knob_value);
	if (is_increased(*knob_value, *prev_knob_value)) {
		*element += change;
		*element = *element > 255 ? 0 : *element;
		*element = *element / 4 * 4;
		*prev_knob_value = *knob_value;
	} else if (is_decreased(*knob_value, *prev_knob_value)) {
		*element = *element - change < 0 ? 255 : *element - change;
		*element = *element / 4 * 4;
		*prev_knob_value = *knob_value;
	}
}

void update_rgb_value(knobs_values_t* knobs_values, knobs_values_t* prev_knobs_values, rgb_value_t* rgb_value) {
	if (knobs_values->red_knob != prev_knobs_values->red_knob) {
		update_element(&(knobs_values->red_knob), &(prev_knobs_values->red_knob), &(rgb_value->red));
	}
	if (knobs_values->green_knob != prev_knobs_values->green_knob) {
		update_element(&(knobs_values->green_knob), &(prev_knobs_values->green_knob), &(rgb_value->green));
	}
	if (knobs_values->blue_knob != prev_knobs_values->blue_knob) {
		update_element(&(knobs_values->blue_knob), &(prev_knobs_values->blue_knob), &(rgb_value->blue));
	}
}

bool is_increased(char value, char prev_value) {
	return (((value - prev_value > STEP) && !(value > 200 && prev_value < 50)) || (value < 50 && prev_value > 200));
}

bool is_decreased(char value, char prev_value) {
	return (((prev_value - value > STEP) && !(value < 50 && prev_value > 200)) || (value > 200 && prev_value < 50));
}


void select_button(menu_t *menu, int green_knob, int *green_knob_prev) {
	int selected = menu->selected;
	int buttons_num = menu->buttons_num;
	//printf("%d %d\n", green_knob, *green_knob_prev);
	if (is_increased(green_knob, *green_knob_prev)) {
		selected = (selected + 1) % buttons_num;
		*green_knob_prev = green_knob;
	} else if (is_decreased(green_knob, *green_knob_prev)){
		selected = selected - 1 < 0 ? buttons_num - 1 : selected - 1;
		*green_knob_prev = green_knob;
	}
	menu->selected = selected;
}

void copy_led_color(led_t* from, led_t* to) {
	to->static_light.rgb_value1 = from->static_light.rgb_value1;
	to->static_light.hsv_value1 = from->static_light.hsv_value1;
}

uint16_t get_rgb565(rgb_value_t rgb_value) {
	uint16_t b = (rgb_value.blue >> 3);
	uint16_t g = ((rgb_value.green >> 2)) << 5;
	uint16_t r = ((rgb_value.red >> 3)) << 11;

	return r | g | b;
}

void convert_hsv_to_rgb(hsv_value_t hsv, rgb_value_t* rgb_value) {
	double r = 0, g = 0, b = 0;
	double h = (double)hsv.h;
	double s = (double)hsv.s / 100;
	double v = (double)hsv.v / 100;
	if (s == 0)
	{
		r = s;
		g = s;
		b = s;
	}
	else
	{
		int i;
		double f, p, q, t;
		if (h == 360)
			h = 0;
		else
			h = h / 60;
		i = (int)trunc(h);
		f = h - i;
		p = v * (1.0 - s);
		q = v * (1.0 - (s * f));
		t = v * (1.0 - (s* (1.0 - f)));
		switch (i)
		{
		case 0:
			r = v;
			g = t;
			b = p;
			break;

		case 1:
			r = q;
			g = v;
			b = p;
			break;

		case 2:
			r = p;
			g = v;
			b = t;
			break;

		case 3:
			r = p;
			g = q;
			b = v;
			break;

		case 4:
			r = t;
			g = p;
			b = v;
			break;

		default:
			r = v;
			g = p;
			b = q;
			break;
		}
	}
	rgb_value->red = r * 255;
	rgb_value->green = g * 255;
	rgb_value->blue = b * 255;

}

hsv_value_t convert_rgb_to_hsv(rgb_value_t rgb) {
	double delta, min;
	double h = 0, s, v;

	min = min(min(rgb.red, rgb.green), rgb.blue);
	v = max(max(rgb.red, rgb.green), rgb.blue);
	delta = v - min;

	if (v == 0.0)
		s = 0;
	else
		s = delta / v;

	if (s == 0)
		h = 0.0;

	else
	{
		if (rgb.red == v)
			h = (rgb.green - rgb.blue) / delta;
		else if (rgb.green == v)
			h = 2 + (rgb.blue - rgb.red) / delta;
		else if (rgb.blue == v)
			h = 4 + (rgb.red - rgb.green) / delta;

		h *= 60;

		if (h < 0.0)
			h = h + 360;
	}

	hsv_value_t hsv;
	hsv.h = h;
	hsv.s = s * 100;
	hsv.v = v / 255 * 100;
	/*printf("h : %f\n", h);
	printf("s : %f\n", s);
	printf("v : %f\n", v);
	printf("H : %d\n", hsv.h);
	printf("S : %d\n", hsv.s);
	printf("V : %d\n", hsv.v);*/

	return hsv;
}
