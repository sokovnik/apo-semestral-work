#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "lcdframe.h"
#include "font_types.h"

unsigned char *parlcd_mem_base = NULL;
uint16_t frame[FRAME_H][FRAME_W];

void frameToLCD() {
	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CMD_o) = 0x2c;
	volatile uint32_t* ptr = (volatile uint32_t*) frame;
	volatile uint32_t* dst = (volatile uint32_t*) (parlcd_mem_base + PARLCD_REG_DATA_o);
	int i;
	for (i = 0; i < FRAME_H*(FRAME_W/2); i++) {
		*dst = *ptr++;
	}
}

int charToFrame(char c, int yRow, int xColumn, uint16_t forecolor, uint16_t backcolor) {
	int cIdx = c - ' ';
	int w = font_winFreeSystem14x16.width[cIdx] + 4;
	int y, x;
	for (y = 0; y < 16; y++) {
		uint16_t mask = font_winFreeSystem14x16.bits[16*cIdx+y];
		for (x = 0; x < w; x++) {
			frame[yRow+y][xColumn + x] = (mask & 0x8000) ? forecolor : backcolor;
			mask <<= 1;
		}
	}
	return w;
}

int bigCharToFrame(char c, int yRow, int xColumn, uint16_t forecolor, uint16_t backcolor)
{
	int cIdx = c - ' ';
	int w = font_winFreeSystem14x16.width[cIdx] * 2 + 4;
	for (int y = 0; y < 32; y+=2) {
		uint16_t mask = font_winFreeSystem14x16.bits[16*cIdx+y/2];
		for (int x = 0; x < w; x+=2) {
			uint16_t color = (mask & 0x8000) ? forecolor : backcolor;
			frame[yRow+y][xColumn+x] = color;
			frame[yRow+y+1][xColumn+x] = color;
			frame[yRow+y][xColumn+x+1] = color;
			frame[yRow+y+1][xColumn+x+1] = color;
			mask <<= 1;
		}
	}
	return w;
}

int strToFrame(char *str, int yRow, int xColumn, uint16_t forecolor, uint16_t backcolor, bool is_big) {
	char c;
	int w = 0;
	if (is_big) {
		while((c=*str++) != 0) {
			w += bigCharToFrame(c, yRow, xColumn+w, forecolor, backcolor);
		}
	} else {
		while((c=*str++) != 0) {
			w += charToFrame(c, yRow, xColumn+w, forecolor, backcolor);
		}
	}
	return w;
}

void rectangleToFrame(rectangle_t rec) {
	for (int i = rec.y; i < rec.y + rec.height; i++) {
		for (int j = rec.x; j < rec.x + rec.width; j++) {
			frame[i][j] = rec.color;
		}
	}
}

void draw_background() {
	volatile uint32_t* ptr = (volatile uint32_t*) frame;
	for (int i = 0; i < FRAME_H * FRAME_W / 2; i++) {
		*ptr++ = BACKGROUND_COLOR;
	}
}

void draw_select_button(int y_shift) {
	strToFrame("Select",y_shift + 280, 380, TEXT_COLOR, INTERFACE_COLOR, is_big_font);
}

void draw_back_button(int y_shift) {
	strToFrame("Back",y_shift + 280, 10, TEXT_COLOR, INTERFACE_COLOR, is_big_font);
}

rectangle_t create_rectangle(int x, int y, int width, int height, uint16_t color) {
	rectangle_t rec;
	rec.x = x;
	rec.y = y;
	rec.width = width;
	rec.height = height;
	rec.color = color;
	return rec;
}

void draw_interface() {
	rectangle_t header = create_rectangle(0, 0, FRAME_W, 40, INTERFACE_COLOR);
	rectangleToFrame(header);
	rectangle_t rectangle;
	uint16_t color;
	for (int i = 0; i < 6; i++) {
		color = i % 2  == 0 ? FIRST_COLOR : SECOND_COLOR;
		rectangle = create_rectangle(0, 40 + i * 40, FRAME_W, 40, color);
		rectangleToFrame(rectangle);
	}
	rectangle_t buttons = create_rectangle(0, 280, FRAME_W, 40, INTERFACE_COLOR);
	rectangleToFrame(buttons);

}
