#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdlib.h>
#include <math.h>

#include "lcdframe.h"

#define STEP 3
#define CHANGE 5

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

typedef struct {
	uint32_t knobs_value;
	int red_knob;
	int green_knob;
	int blue_knob;
	int red_button;
	int green_button;
	int blue_button;
} knobs_values_t;

typedef struct {
	char* name;
	char* buttons[6];
	int buttons_num;
	int selected;
} menu_t;

typedef struct {
	int h;
	int s;
	int v;
} hsv_value_t;

typedef struct {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
} rgb_value_t;

typedef struct {
	hsv_value_t hsv_value1;
	hsv_value_t hsv_value2;
	rgb_value_t rgb_value1;
	rgb_value_t rgb_value2;
	rgb_value_t cur_rgb_value;
	hsv_value_t cur_hsv_value;
	int change_time;
	double prev_change_time;
	double prev_change_time_h;
	double prev_change_time_s;
	double prev_change_time_v;
	bool color_changed;
	bool on;
	int duration_time;
	int extinction_time;

} mode_type;

typedef struct {
	hsv_value_t cur_hsv_value;
	rgb_value_t cur_rgb_value;
	mode_type static_light;
	mode_type color_flashing;
	mode_type flashing;
	mode_type cont_change;
	unsigned char *led_mem;
	bool mode_changed;

} led_t;

led_t led1, led2, led1_saved, led2_saved;

void update_hsv_value(knobs_values_t* knobs_values, knobs_values_t* prev_knobs_values, hsv_value_t* hsv_value);
void update_rgb_value(knobs_values_t* knobs_values, knobs_values_t* prev_knobs_values, rgb_value_t* rgb_value);
void select_button(menu_t *menu, int green_knob, int *green_knob_prev);
void copy_led_color(led_t* from, led_t* to);
uint16_t get_rgb565(rgb_value_t rgb_value);
void convert_hsv_to_rgb(hsv_value_t hsv, rgb_value_t* rgb_value);
hsv_value_t convert_rgb_to_hsv(rgb_value_t rgb);
bool is_increased(char value, char prev_value);
bool is_decreased(char value, char prev_value);
void initialize_mode(mode_type *mode);
void initialize_led(led_t *led);
void set_led_color(led_t *led);
void light_off(led_t *led);

#endif
