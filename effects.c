#include "effects.h"

void color_flash (led_t* led) {
	double time = (double)clock() / CLOCKS_PER_SEC * 1000;
	if (time - led->color_flashing.prev_change_time > led->color_flashing.change_time) {
		if (!led->color_flashing.color_changed) {
			led->cur_rgb_value = led->color_flashing.rgb_value1;
			led->cur_hsv_value = led->color_flashing.hsv_value1;
			led->color_flashing.color_changed = true;
		} else {
			led->cur_rgb_value = led->color_flashing.rgb_value2;
			led->cur_hsv_value = led->color_flashing.hsv_value2;
			led->color_flashing.color_changed = false;
		}
		led->color_flashing.prev_change_time = time;
	}
}

void flash (led_t* led) {
	double time = (double)clock() / CLOCKS_PER_SEC * 1000;
	if (!(led->flashing.color_changed)) {
		if (time - led->flashing.prev_change_time > led->flashing.duration_time 
		&& led->flashing.duration_time != 0) {
			led->flashing.color_changed = true;
			led->flashing.prev_change_time = time;
		}
	} else {
		if (time - led->flashing.prev_change_time > led->flashing.extinction_time
		&& led->flashing.duration_time != 0) {
			led->flashing.color_changed = false;
			led->flashing.prev_change_time = time;
		}
		led->cur_rgb_value.red = 0;
		led->cur_rgb_value.green = 0;
		led->cur_rgb_value.blue = 0;
		led->cur_hsv_value.h = 0;
		led->cur_hsv_value.s = 0;
		led->cur_hsv_value.v = 0;
	}
}

void change(led_t* led) {
	int step = 1;
	//h
	double change_time = led->cont_change.change_time;
	change_time /= abs(led->cont_change.hsv_value1.h - led->cont_change.hsv_value2.h);
	step = led->cont_change.hsv_value2.h - led->cont_change.hsv_value1.h > 0 ? 1 : -1;
	double time = (double)clock() / CLOCKS_PER_SEC * 1000;
	if (time - led->cont_change.prev_change_time_h > change_time) {
		if (!led->cont_change.color_changed) {
			led->cont_change.cur_hsv_value.h += step;
		} else {
			led->cont_change.cur_hsv_value.h -= step;
		}
		if (led->cont_change.cur_hsv_value.h == led->cont_change.hsv_value2.h) {
			led->cont_change.color_changed = !led->cont_change.color_changed;
			return;
		}
		if (led->cont_change.cur_hsv_value.h == led->cont_change.hsv_value1.h) {
			led->cont_change.color_changed = !led->cont_change.color_changed;
			return;
		}
		led->cont_change.prev_change_time_h = time;
	}
	//s
	change_time = led->cont_change.change_time;
	change_time /= abs(led->cont_change.hsv_value1.s - led->cont_change.hsv_value2.s);
	step = led->cont_change.hsv_value2.s - led->cont_change.hsv_value1.s > 0 ? 1 : -1;
	if (time - led->cont_change.prev_change_time_s > change_time) {
		if (!led->cont_change.color_changed) {
			led->cont_change.cur_hsv_value.s += step;
		} else {
			led->cont_change.cur_hsv_value.s -= step;
		}
		if (led->cont_change.cur_hsv_value.s == led->cont_change.hsv_value2.s) {
			led->cont_change.color_changed = !led->cont_change.color_changed;
			return;
		}
		if (led->cont_change.cur_hsv_value.s == led->cont_change.hsv_value1.s) {
			led->cont_change.color_changed = !led->cont_change.color_changed;
			return;
		}
		led->cont_change.prev_change_time_s = time;
	}
	//v
	change_time = led->cont_change.change_time;
	change_time /= abs(led->cont_change.hsv_value1.v - led->cont_change.hsv_value2.v);
	step = led->cont_change.hsv_value2.v - led->cont_change.hsv_value1.v > 0 ? 1 : -1;
	if (time - led->cont_change.prev_change_time_v > change_time) {
		if (!led->cont_change.color_changed) {
			led->cont_change.cur_hsv_value.v += step;
		} else {
			led->cont_change.cur_hsv_value.v -= step;
		}
		if (led->cont_change.cur_hsv_value.v == led->cont_change.hsv_value2.v) {
			led->cont_change.color_changed = !led->cont_change.color_changed;
			return;
		}
		if (led->cont_change.cur_hsv_value.v == led->cont_change.hsv_value1.v) {
			led->cont_change.color_changed = !led->cont_change.color_changed;
			return;
		}
		led->cont_change.prev_change_time_v = time;
	}

	convert_hsv_to_rgb(led->cont_change.cur_hsv_value, &(led->cont_change.cur_rgb_value));
	led->cur_hsv_value = led->cont_change.cur_hsv_value;
	led->cur_rgb_value = led->cont_change.cur_rgb_value;
	//printf("%d %d %d\n", led->cur_hsv_value.h, led->cur_hsv_value.s, led->cur_hsv_value.v);
}
